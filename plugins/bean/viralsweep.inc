<?php
/**
 * @file
 * Viralsweep bean plugin.
 */

class ViralsweepBean extends BeanPlugin {
  /**
   * Declares default block settings.
   */
  public function values() {
    return array(
      'settings' => array(
        'cnt_id' => '',
        'embed_format' => '',
      ),
    );
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {
    $form = array();
    $form['settings'] = array(
      '#type' => 'fieldset',
      '#tree' => 1,
      '#title' => t('Options'),
    );

    $form['settings']['cnt_id'] = array(
      '#type' => 'textfield',
      '#title' => t('CNT ID'),
      '#description' => t('ID of the sweep to embed.'),
      '#size' => 25,
      '#required' => TRUE,
      '#default_value' => isset($bean->settings['cnt_id']) ? $bean->settings['cnt_id'] : '',
    );

    $form['settings']['embed_format'] = array(
      '#type' => 'select',
      '#title' => t('Embed Format'),
      '#description' => t('ID of the sweep to embed.'),
      '#options' => array(
        'full_page' => 'Full Page',
        'widget' => 'Widget',
        'lightbox' => 'Light Box',
        'popup' => 'Pop-up',
      ),
      '#required' => TRUE,
      '#default_value' => isset($bean->settings['embed_format']) ? $bean->settings['embed_format'] : '',
    );

    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $cnt_id = check_plain($bean->settings['cnt_id']);
    $embed_format = $bean->settings['embed_format'];
    $content['viralsweep']['#markup'] = viralsweep_render($cnt_id, $embed_format);
    return $content;
  }
}
